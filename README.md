# cisco_create_acl

This project will help automate granularizing overly permissive rules on IAD Cisco Firewall.

Normally it's done with the following steps:

1- Find out the traffic passing through the ACL:
`cat messages |grep access-list  | sed -e 's/^.*permitted//' -e 's/hit-cnt.*$//' | sed -e 's/([0-9][0-9]*) ->/ ->/' | sort |uniq -c | sort -rn | head -50`

2- Go through the src/dst IP's 

3- Go through access-list names

4- Write new rule sets

With this code we only plug in the file that receives firewall traffic and it will generate the necessary more secure rules.

example:


I want to see top X most frequent traffic:5

#################*******************#################*******************#################
#################*******************#################*******************#################

Please create below ACLs

['access-list RANDOM-FILTER extended permit udp host 192.168.2.11 eq 53']
