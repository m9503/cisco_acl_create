FROM python:3.9-slim-buster

LABEL org.opencontainers.image.authors="Firuz Dumlupinar"

RUN mkdir -p /opt/parser
WORKDIR /opt/parser
COPY parser/* /opt/parser/

RUN addgroup --gid 9000 app-user \
    && adduser -D -h /opt/parser -u 9000 -G app-user -s /bin/sh app-user \
    && pip3 --no-cache-dir install termcolor

USER app-user

ENTRYPOINT ["python3"]

CMD ["/opt/parser/cisco_parsing.py"]
