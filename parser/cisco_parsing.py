import re
import collections
import pprint
from termcolor import colored

separator = "#################*******************#################*******************#################"
colored_text = colored(separator, color="red")


def create_acl(pattern):
    # pattern is a list of top n sample traffic.
    # Each element is a string
    acl = []  # This will contain the acl's to be created.
    subnets = ["172.28.44",
                         "172.28.32"]
    pos_1 = 3  # Position of src IP when access-list is not XXX
    pos_2 = 6  # Position of src IP when access-list is XXX

    for traffic in pattern:
        if "XXX" not in traffic:
            num = pos_1
        else:
            num = pos_2
        src_ip = traffic.split(" ")[num].split("/")[1]
        dst_ip = traffic.split(" ")[num + 2].split("/")[1].split("(")[0]
        port = traffic.split(" ")[num + 2].split("/")[1].split("(")[1].split(")")[0]
        protocol = traffic.split(" ")[2]
        access_list_name = traffic.split(" ")[0]


        if "XXX" in access_list_name:
            if src_ip[:9] in subnets: # Cisco has a certain way of writing VPN-FILTER rules
                rule = f"access-list {access_list_name} extended permit {protocol} host {dst_ip} eq {port} {src_ip} "
            else:
                rule = f"access-list {access_list_name} extended permit {protocol} host {src_ip} {dst_ip} eq {port} "
        else:
            rule = f"access-list {access_list_name} extended permit {protocol} host {src_ip} {dst_ip} eq {port}"
        acl.append(rule)
    print(colored_text)
    print("Please create below ACLs... ")
    pprint.pprint(acl)


def parse_traffic(filename, num):
    with open(filename) as f:
        data = f.readlines()
        data_cleaner = []
        for line in data:
            if "permitted" in line:
                sub1 = re.sub(".*access-list ", "", line)
                sub2 = re.sub("hit-cnt.*$", "", sub1)
                sub3 = re.sub("\([0-9][0-9]*\) ->", " ->", sub2)
                data_cleaner.append(sub3)
    counter = collections.Counter(data_cleaner)
    final_list = counter.most_common(num)
    print(colored_text)
    pprint.pprint(final_list)

    most_common_traffic = [item[0] for item in final_list]
    create_acl(most_common_traffic)


def main():
    while True:
        try:
            num = int(input("I want to see top X most frequent traffic:"))
            if num < 1 or num > 100:
                raise ValueError
            break
        except ValueError:
            print("Please enter a valid number between 1-100...")
    filename = "/tmp/messages"
    parse_traffic(filename, num)


if __name__ == '__main__':
    main()
